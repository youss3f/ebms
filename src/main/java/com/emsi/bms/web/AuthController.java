package com.emsi.bms.web;

import com.emsi.bms.entity.ERole;
import com.emsi.bms.entity.Role;
import com.emsi.bms.entity.User;
import com.emsi.bms.payload.request.LoginRequestPayload;
import com.emsi.bms.payload.request.SignupRequestPayload;
import com.emsi.bms.payload.response.JwtResponsePayload;
import com.emsi.bms.payload.response.MessageResponsePayload;
import com.emsi.bms.repository.RoleRepository;
import com.emsi.bms.repository.UserRepository;
import com.emsi.bms.sec.JwtUtils;
import com.emsi.bms.sec.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@CrossOrigin(value = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private JwtUtils jwtUtils;

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequestPayload loginRequestPayload) {
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                loginRequestPayload.getUsername(), loginRequestPayload.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList());
        return ResponseEntity.ok(new JwtResponsePayload(jwt, userDetails.getId(), userDetails.getUsername(),
                userDetails.getEmail(), roles));
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequestPayload signupRequestPayload) {
        if (userRepository.existsByUsername(signupRequestPayload.getUsername()))
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponsePayload("Error: Username is already taken!"));
        if (userRepository.existsByEmail(signupRequestPayload.getEmail()))
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponsePayload("Error: Email is already in use!"));

        // create users account
        User user = new User(signupRequestPayload.getUsername(), signupRequestPayload.getEmail(),
                passwordEncoder.encode(signupRequestPayload.getPassword()));

        Set<String> strRoles = signupRequestPayload.getRole();
        Set<Role> roles = new HashSet<>();
        if (strRoles == null) {
            Role userRole = roleRepository.findByRoleName(ERole.ROLE_USER).orElseThrow(() ->
                new RuntimeException("Error: Role is not found."));
            roles.add(userRole);
        } else {
            strRoles.forEach(role -> {
                switch (role) {
                    case "admin":
                        Role adminRole = roleRepository.findByRoleName(ERole.ROLE_ADMIN)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(adminRole);
                        break;
                    default:
                        Role userRole = roleRepository.findByRoleName(ERole.ROLE_USER)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(userRole);
                }
            });
        }
        user.setRoles(roles);
        userRepository.save(user);
        return ResponseEntity.ok(new MessageResponsePayload("User registered successfully"));
    }
}
