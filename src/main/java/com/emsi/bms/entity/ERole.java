package com.emsi.bms.entity;

public enum ERole {
    ROLE_USER,
    ROLE_ADMIN
}
