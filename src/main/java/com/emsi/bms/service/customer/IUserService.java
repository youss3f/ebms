package com.emsi.bms.service.customer;

import com.emsi.bms.entity.Account;
import com.emsi.bms.entity.User;

import java.util.List;

public interface IUserService {

    List<User> getAllUsers();
    User addUser(User user);
    User getUserById(Long userId);
    void deleteUserById(Long userId);
    List<Account> getUserAccounts(Long userId);
}
