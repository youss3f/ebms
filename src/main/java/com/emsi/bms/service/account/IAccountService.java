package com.emsi.bms.service.account;

import com.emsi.bms.entity.Account;
import com.emsi.bms.entity.Transaction;

import java.util.Collection;
import java.util.List;

public interface IAccountService {

    List<Account> getAllAccounts();
    Account getAccountById(Long accountId);
    void deleteAccount(Long accountId);
    List<Transaction> getAccountTransactions(Long accountId);
}
