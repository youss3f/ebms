package com.emsi.bms.service.bank;

import com.emsi.bms.entity.Bank;

import java.util.List;

public interface IBankService {

    List<Bank> getAllBanks();
    Bank addBank(Bank bank);
    Bank getBankById(Long bankId);
    Bank updateBank(Long bankId, Bank newBank);
    void deleteBankById(Long bankId);
}
